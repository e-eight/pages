build:
	emacs -Q --batch --script publish.el

clean:
	rm ~/.org-timestamps/org-*

clean_and_build: | clean build
