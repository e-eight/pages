<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-10-07 Mon 23:20 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>papers+.el</title>
<meta name="generator" content="Org Mode" />
<link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>স</text></svg>">
<link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
<link rel="stylesheet" type="text/css" href="/css/syntax.css">
<link rel="stylesheet" type="text/css" href="/css/extra.css">
</head>
<body>
<header id="preamble" class="status">
<nav>
<a href="/" alt="Go home">home</a>
<a href="/posts/" alt="Read all posts">posts</a>
</nav>
<h1 class="title">papers+.el</h1>
<p class="date">Published on 2024-09-28 (updated on 2024-10-07)</p>
<p class="tags">tags: <a class="tag" href="../../tags/emacs/index.html">emacs</a> <a class="tag" href="../../tags/org/index.html">org</a></p>
</header>
<main id="content" class="content">
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org5e15436">The why and the what</a></li>
<li><a href="#org5c2a81e">The how</a>
<ul>
<li><a href="#org0d17bce">Customization options</a></li>
<li><a href="#org66d03b8">arXiv backend</a></li>
<li><a href="#org7a8a10b">Local PDF backend</a></li>
<li><a href="#org399edf0">Frontend</a></li>
<li><a href="#org430f0fa">init.el</a></li>
</ul>
</li>
</ul>
</div>
</nav>

<main id="outline-container-org5e15436" class="outline-2">
<h2 id="org5e15436">The why and the what</h2>
<div class="outline-text-2" id="text-org5e15436">
<p>
Org is a fantastic tool, the office suite for the Emacs OS. I maintain this website with Org, even wrote my dissertation with it. Aided by packages like <a href="https://github.com/jkitchin/org-ref"><code>org-ref</code></a>, <a href="https://github.com/emacs-citar/citar">citar</a>, <a href="https://github.com/cdominik/cdlatex"><code>cdlatex</code></a>, writing scientific documents in Org is <i>almost</i> a breeze. Writing my dissertation, I realized that just like Emacs is not the ideal OS for every circumstance (never used it as a OS myself), Org is not the ideal office suite for all circumstances. I prefer writing equation-heavy articles directly in LaTeX than doing the same in Org. Even when Org is beefed up with all those supporting packages. Particularly when I have to collaborate with co-authors who do not use Emacs, let alone Org.
</p>

<p>
Another issue that I faced writing scientific documents in Org is that updates to Org (or supporting packages) can introduce changes to the exporter that force me to tweak the Org file to properly output to a PDF. In my experience, this is much rare when writing directly in LaTeX.
</p>

<p>
However, there are multiple steps, before you start writing a scientific article, for which Org is still a fantastic tool. Primary among them are taking notes, making presentations, and collecting references for the bibliography of your article. This post is about a small package I wrote, creatively called <code>papers+</code>, to easily add papers to a central list. <code>org-ref</code> also provides a similar ability, but that is just one of the many things that it does. As mentioned above, the other functionalities of <code>org-ref</code> are not very useful to me. So why fill up my <code>elpa</code> directory with dependency packages that I do not need?
</p>

<p>
<code>papers+</code> does one thing and one thing only &#x2014; add an entry corresponding to a paper to a central Org file. The central Org file acts as a repository for all your papers. You can easily search it with either the built-in <code>isearch</code> or something like <code>consult-ripgrep</code>. Each entry consists of the following:
</p>
<ol class="org-ol">
<li>The title of the paper, as an Org header.</li>
<li>BibTeX citation for the paper.</li>
</ol>
<p>
An entry in my central Org file looks like the following:
</p>
<div class="org-src-container">
<pre class="src src-org"><span class="org-org-level-2">** </span><span class="org-org-level-2"><span class="org-org-link"><a href="file:1906.12122.pdf">Nuclear effective field theory: status and perspectives</a></span></span>
<span class="org-org-block-begin-line">  #+begin_src bibtex</span>
<span class="org-org-block">  </span><span class="org-org-block"><span class="org-function-name">@article</span></span><span class="org-org-block">{</span><span class="org-org-block"><span class="org-constant">Hammer:2019poc</span></span><span class="org-org-block">,</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">author</span></span><span class="org-org-block"> = {Hammer, H. -W. and K\"onig, S. and van Kolck, U.},</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">title</span></span><span class="org-org-block"> = "{Nuclear effective field theory: status and perspectives}",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">eprint</span></span><span class="org-org-block"> = "1906.12122",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">archivePrefix</span></span><span class="org-org-block"> = "arXiv",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">primaryClass</span></span><span class="org-org-block"> = "nucl-th",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">doi</span></span><span class="org-org-block"> = "</span><span class="org-org-block"><span class="org-button">10.1103/RevModPhys.92.025004</span></span><span class="org-org-block">",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">journal</span></span><span class="org-org-block"> = "Rev. Mod. Phys.",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">volume</span></span><span class="org-org-block"> = "92",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">number</span></span><span class="org-org-block"> = "2",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">pages</span></span><span class="org-org-block"> = "025004",</span>
<span class="org-org-block">      </span><span class="org-org-block"><span class="org-variable-name">year</span></span><span class="org-org-block"> = "2020"</span>
<span class="org-org-block">  }</span>
<span class="org-org-block-end-line">  #+end_src</span>
</pre>
</div>
<p>
The header is also a link to the PDF file corresponding to the paper. If you hover over the header in the source block above, you will see that it is a link to a (nonexistent) file. In Emacs, <code>C-c C-o</code> on the header opens the PDF file.
</p>

<p>
Currently, <code>papers+</code> has two methods to add papers:
</p>
<ol class="org-ol">
<li>From an arXiv link.</li>
<li>From a local PDF.</li>
</ol>
<p>
When adding from an arXiv link, <code>papers+</code> will do the following:
</p>
<ol class="org-ol">
<li>Download the PDF of the paper to a central location.</li>
<li>Ask you to enter the title of the paper. You can give the paper&rsquo;s original title as the title, or something else if you prefer. I sometimes do the latter.</li>
<li>Try to get the BibTeX citation from the InspireHEP database. Failing which, it ask you to enter the BibTeX citation.</li>
<li>Convert all of that to an Org capture, that you can edit before adding it to the central Org file. A trick I took from <a href="https://github.com/oantolin/emacs-config/blob/master/my-lisp/arXiv.el">oantolin&rsquo;s arXiv.el</a>. You must add an entry for <code>papers+</code> in your <code>org-capture-templates</code> for this to work.</li>
</ol>
<p>
Adding from a local PDF is very similar, except that it will copy the PDF to the central location, and will always ask you to enter the BibTeX citation.
</p>

<p>
This is not a fully automatic solution. But it is usually automatic enough for my purposes. There are also edge cases. I have not implemented any method to check for duplicates. But usually it is not an issue. <code>papers+</code> is quite extendable though. You can make it more automatic if you choose. For example, you can parse the output from the arXiv API to automatically get the title. Personally, I found that implementing an arXiv API parser in Elisp is far more of a time sink than just manually entering the title. You can also easily add other sources to add papers/citations from, or checks for duplicates.
</p>

<p>
What did this achieve? Keeping your papers in a central Org file allows you to do (at least) the following:
</p>
<ol class="org-ol">
<li>Tangle all the BibTeX source blocks to easily create a <code>.bib</code> file that you can use in your LaTeX document.</li>
<li>Refile the entries in your central Org file to better organize your papers, however you want. You can even refile the entries to other Org files if you want. If you no longer need a paper, you can just delete that entry.</li>
<li>Easily add notes to each entry. This is useful to keep track of which paper has which information.</li>
<li>It is quite easy to search the Org file. It will take a while for the file to grow to unmanageable sizes, and even then searching with tools like <code>consult-ripgrep</code> or <code>org-ql</code> might still be a breeze.</li>
</ol>
</div>
</main>

<main id="outline-container-org5c2a81e" class="outline-2">
<h2 id="org5c2a81e">The how</h2>
<div class="outline-text-2" id="text-org5c2a81e">
<p>
Here is the good part. Feel free to use the code as is or modify it however you want.
</p>
</div>

<div id="outline-container-org0d17bce" class="outline-3">
<h3 id="org0d17bce">Customization options</h3>
<div class="outline-text-3" id="text-org0d17bce">
<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span class="org-keyword">require</span> '<span class="org-constant">org-capture</span>)

(<span class="org-keyword">defgroup</span> <span class="org-type">papers+</span> nil
  <span class="org-doc">"Utility functions for maintaining research papers."</span>
  <span class="org-builtin">:group</span> 'applications)

(<span class="org-keyword">defcustom</span> <span class="org-variable-name">papers-dir+</span> <span class="org-string">"~/Documents/papers/"</span>
  <span class="org-doc">"Directory where papers are stored."</span>
  <span class="org-builtin">:type</span> 'file
  <span class="org-builtin">:group</span> 'papers+)

(<span class="org-keyword">defcustom</span> <span class="org-variable-name">papers-org-capture-file+</span>
  (expand-file-name <span class="org-string">"reference.org"</span> papers-dir+)
  <span class="org-doc">"List of papers."</span>
  <span class="org-builtin">:type</span> 'file
  <span class="org-builtin">:group</span> 'papers+)

(<span class="org-keyword">defcustom</span> <span class="org-variable-name">papers-org-capture-header+</span>
  <span class="org-string">"To be refiled"</span>
  <span class="org-doc">"Header for org-capture."</span>
  <span class="org-builtin">:type</span> 'string
  <span class="org-builtin">:group</span> 'papers+)

(<span class="org-keyword">defcustom</span> <span class="org-variable-name">papers-org-capture-key+</span>
  <span class="org-string">"p"</span>
  <span class="org-doc">"Key for org-capture."</span>
  <span class="org-builtin">:type</span> 'key
  <span class="org-builtin">:group</span> 'papers+)

(<span class="org-keyword">defcustom</span> <span class="org-variable-name">paper-sources+</span>
  '((<span class="org-string">"arXiv"</span> . papers--arXiv-add+)
    (<span class="org-string">"local"</span> . papers--local-add+))
  <span class="org-doc">"Sources for papers."</span>
  <span class="org-builtin">:type</span> '(alist <span class="org-builtin">:key-type</span> string <span class="org-builtin">:value-type</span> function)
  <span class="org-builtin">:group</span> 'papers+)
</pre>
</div>
</div>
</div>

<div id="outline-container-org66d03b8" class="outline-3">
<h3 id="org66d03b8">arXiv backend</h3>
<div class="outline-text-3" id="text-org66d03b8">
<div class="org-src-container">
<pre class="src src-elisp">(<span class="org-keyword">defun</span> <span class="org-function-name">papers--arXiv-id+</span> (url-or-id)
  <span class="org-doc">"Extract arXiv ID from URL-OR-ID."</span>
  (<span class="org-keyword">cond</span>
   ((string-match
     <span class="org-string">"^https?:</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/arxiv.org/</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(?:</span></span><span class="org-string">abs</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">pdf</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(</span></span><span class="org-string">[a-z-</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">[0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string">"</span>
     url-or-id)
    (match-string 1 url-or-id))
   ((string-match <span class="org-string">"</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(</span></span><span class="org-string">[a-z-</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">[0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string">"</span> url-or-id)
    (match-string 1 url-or-id))))

(<span class="org-keyword">defun</span> <span class="org-function-name">papers--arXiv-url+</span> (url-or-id)
  <span class="org-doc">"Construct arXiv pdf url from URL-OR-ID."</span>
  (<span class="org-keyword">cond</span>
   ((string-match
     <span class="org-string">"^https?:</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/arxiv.org/</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(?:</span></span><span class="org-string">abs</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">pdf</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(</span></span><span class="org-string">[a-z-</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">[0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string">"</span>
     url-or-id 0 t)
    (string-replace <span class="org-string">"abs"</span> <span class="org-string">"pdf"</span> url-or-id))
   ((string-match <span class="org-string">"</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">(</span></span><span class="org-string">[a-z-</span><span class="org-string"><span class="org-warning">\</span></span><span class="org-string">/0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">|</span></span><span class="org-string">[0-9v.]+</span><span class="org-string"><span class="org-regexp-grouping-backslash">\\</span></span><span class="org-string"><span class="org-regexp-grouping-construct">)</span></span><span class="org-string">"</span> url-or-id 0 t)
    (concat <span class="org-string">"https://arxiv.org/pdf/"</span> url-or-id))))

(<span class="org-keyword">defun</span> <span class="org-function-name">papers--arXiv-download+</span> (url-or-id)
  <span class="org-doc">"Download paper from URL-OR-ID."</span>
  (<span class="org-keyword">let</span> ((pdf-url (papers--arXiv-url+ url-or-id))
        (pdf-file
         (expand-file-name
          (format <span class="org-string">"%s.pdf"</span>
           (string-replace <span class="org-string">"/"</span> <span class="org-string">"."</span> (papers--arXiv-id+ url-or-id)))
          papers-dir+)))
    (<span class="org-keyword">unless</span> (file-directory-p papers-dir+)
      (make-directory papers-dir+ t))
    (<span class="org-keyword">unless</span> (file-exists-p pdf-file)
      (url-copy-file pdf-url pdf-file 1))
    pdf-file))

(<span class="org-keyword">defun</span> <span class="org-function-name">papers--arXiv-inspirehep-bibtex+</span> (url-or-id)
  <span class="org-doc">"Get bibtex data from Inspire HEP for arXiv URL-OR-ID."</span>
  (<span class="org-keyword">with-current-buffer</span>
      (url-retrieve-synchronously
       (format <span class="org-string">"https://inspirehep.net/api/arxiv/%s?format=bibtex"</span>
               (papers--arXiv-id+ url-or-id)))
    (goto-char (point-min))
    (<span class="org-keyword">if</span> (search-forward <span class="org-string">"404"</span> nil t)
        nil
      (<span class="org-keyword">progn</span>
        (goto-char (point-min))
        (search-forward <span class="org-string">"\n\n"</span>)
        (goto-char (match-end 0))
        (delete-region (point-min) (point))
        (string-trim (buffer-string))))))

(<span class="org-keyword">defun</span> <span class="org-function-name">papers--arXiv-add+</span> ()
  (<span class="org-keyword">let*</span> ((url-or-id (read-from-minibuffer <span class="org-string">"Enter arXiv url or id: "</span> nil))
         (title (read-from-minibuffer <span class="org-string">"Enter title: "</span> nil))
         (pdf (papers--arXiv-download+ url-or-id))
         (bib (papers--arXiv-inspirehep-bibtex+ url-or-id)))
    (<span class="org-keyword">unless</span> bib
      (<span class="org-keyword">setq</span> bib (read-from-minibuffer <span class="org-string">"Enter bibtex record: "</span> nil)))
    (list pdf title bib)))
</pre>
</div>
</div>
</div>

<div id="outline-container-org7a8a10b" class="outline-3">
<h3 id="org7a8a10b">Local PDF backend</h3>
<div class="outline-text-3" id="text-org7a8a10b">
<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span class="org-keyword">defun</span> <span class="org-function-name">papers--local-add+</span> ()
  (<span class="org-keyword">let*</span> ((old-pdf (read-file-name <span class="org-string">"Enter path to pdf: "</span>))
         (pdf (expand-file-name (file-name-nondirectory old-pdf) papers-dir+))
         (title (read-from-minibuffer <span class="org-string">"Enter title: "</span> nil))
         (bib (read-from-minibuffer <span class="org-string">"Enter bibtex record: "</span> nil)))
    (rename-file old-pdf pdf)
    (list pdf title bib)))
</pre>
</div>
</div>
</div>

<div id="outline-container-org399edf0" class="outline-3">
<h3 id="org399edf0">Frontend</h3>
<div class="outline-text-3" id="text-org399edf0">
<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span class="org-keyword">defun</span> <span class="org-function-name">papers-add+</span> ()
  (<span class="org-keyword">interactive</span>)
  (<span class="org-keyword">let*</span> ((source
          (completing-read <span class="org-string">"Select source: "</span> (mapcar #'car paper-sources+)))
         (params
          (funcall (cdr (assoc source paper-sources+)))))
    (kill-new
     (format <span class="org-string">"[[file:%s][%s]]\n#+begin_src bibtex\n%s\n#+end_src"</span>
             (file-relative-name (nth 0 params) papers-dir+)
             (nth 1 params)
             (nth 2 params)))
    (org-capture nil papers-org-capture-key+)))

(<span class="org-keyword">provide</span> '<span class="org-constant">papers+</span>)
</pre>
</div>
</div>
</div>

<div id="outline-container-org430f0fa" class="outline-3">
<h3 id="org430f0fa">init.el</h3>
<div class="outline-text-3" id="text-org430f0fa">
<p>
In my <code>init.el</code>, I have the following config for <code>papers+</code>:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span class="org-keyword">use-package</span> papers+
<span class="org-builtin">:config</span>
(<span class="org-keyword">defun</span> <span class="org-function-name">papers-grep+</span> ()
  (<span class="org-keyword">interactive</span>)
  (consult-grep+ papers-dir+))
(<span class="org-keyword">bind-keys</span> <span class="org-builtin">:prefix-map</span> papers+-map
           <span class="org-builtin">:prefix</span> <span class="org-string">"C-M-p"</span>
           <span class="org-builtin">:prefix-docstring</span> <span class="org-string">"Key bindings for papers+."</span>
           (<span class="org-string">"a"</span> . papers-add+)
           (<span class="org-string">"g"</span> . papers-grep+)))
</pre>
</div>
<p>
<code>consult-grep+</code> is a wrapper function that uses <code>consult-ripgrep</code> for grepping if <code>rg</code> is available, else defaults to <code>consult-grep</code>. The <code>bind-keys</code> macro is provided by <code>use-package</code>.
</p>

<p>
I also add the following within my <code>(use-package org ...)</code> block:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span class="org-keyword">setopt</span> org-capture-templates
        `((,papers-org-capture-key+
           <span class="org-string">"Papers"</span>
           entry
           (file+headline
            ,papers-org-capture-file+
            ,papers-org-capture-header+)
           <span class="org-string">"* %c"</span>)))
(<span class="org-keyword">setopt</span> org-refile-targets
        `((,papers-org-capture-file+ . (<span class="org-builtin">:maxlevel</span> . 4))))
</pre>
</div>
<p>
Just to keep things simple, I load Org after loading <code>papers+</code> in my <code>init.el</code>. This ensures that all the customization options in <code>papers+</code> are known to Emacs when I set them in the <code>(use-package org ...)</code> block. 
</p>
</div>
</div>
</main>
</main>
<footer id="postamble" class="status">
<p>Copyright &copy; 2020 &mdash; 2024 Soham Pal</p>
</footer>
</body>
</html>
