#+title: How this website is created
#+date: 2024-04-30
#+filetags: :emacs:org:

It's been a while since I updated this site. Most of the old content was tailored for job applications. I have less reasons now to use this as a portfolio. Rather just a space to host my thoughts. Figured I will rebuild the site from scratch. I primarily write all my posts and notes in [[https://orgmode.org/][Org]]. Earlier I had successively used [[https://ox-hugo.scripter.co/][ox-hugo]] (+ Hugo) and [[https://niklasfasching.github.io/go-org/blorg/][blorg]] convert those to HTML. Nice and fast tools, but didn't neatly fit into my workflow.

This time around I wanted to cut out all the middlemen and just use Emacs and Org to build this site. I have +almost+ succeeded +, but for a minor Python bit+. There are a bazillion blogs and tutorials on using Emacs and Org to build a static site. I found [[https://anonimno.codeberg.page/config.html][this]] and [[https://vincent.demeester.fr/posts/2020-03-22-org-mode-website.html][this]] particularly useful. Crawling [[https://www.reddit.com/r/emacs/][/r/emacs]] will give you dozens more. I have decided on the following to keep things relatively stable, and not reinvent the wheel:
- Use only built-in Emacs packages (I am using version 29+)
- Use a [[https://simplecss.org/][simple css]]
- +Use only Python standard library for the non-Emacs Lisp parts (I am using version 3.11)+ (No more Python dependency!)

This Emacs + Org ++ Python+ publishing system might be slower than some of the more popular static site generators. But in the absence of any benchmarks all I can say is that I really do not see any difference. On the other hand this system is self-contained, stable, and modular. My hope is that now that I have it set up, I will not have to tinker with it much.

#+toc: headlines 1

* Directory structure
The directory structure of this website is as follows (produced with ~M-x speedbar~ which does not show hidden files like ~.htaccess~, etc.):
#+begin_src
0:<+> css
0:<+> images
0:<+> posts
0:<+> tags
0:[+] 404.html *
0:[+] 404.org *
0:[+] Makefile *
0:[+] index.html *
0:[+] index.org *
0:[+] publish.el *
#+end_src
Additional CSS files live in the ~css~ directory:
1. ~extra.css~ adds some styling not handled by ~simplecss~.
2. ~syntax.css~ produced with ~htmlize-buffer~ from ~htmlize.el~ handles code highlighting.
Images used outside of posts, such as my profile picture, live in the ~images~ directory.

** Posts
All posts and any associated files, such as images, live in their own directories under the ~posts~ directory. This setup of the posts makes it very easy to remove any post, and I do not need to worry about links to images and such. At the time of updating this post, the ~posts~ directory looks like as follows:
#+begin_src
0:<-> posts
1: <+> braketx
1: <-> how-this-website-is-created
2:  [+] index.html *
2:  [+] index.org *
1: [+] index.html *
1: [+] index.org *
#+end_src

** Tags
The ~tags~ directory is similarly arranged as the ~posts~ directory, one sub-directory for each tag. Each sub-directory contains an ~index.org~ file which lists all the posts corresponding to that tag. Originally I was using Python to create this, but now I have figured out an Emacs Lisp solution. To be honest I feel that the Emacs Lisp solution is simpler.

My original idea was:
1. Build a hash table with tags as keys and list of corresponding posts as values.
2. Create sub-directories for each tag under ~tags~.
3. Write the contents of the hash table to the ~index.org~ files in the sub-directories under ~tags~.
4. Generate HTML files for the tags with Org publishing.
5. Link each tag HTML file from its corresponding post HTML files.

The last bit required post-processing the generated HTML files. I failed to implement this with Emacs's hash table. Implementing this with Python's ~dict~ and regex searches was simple. But it was a fragile system. Changes, whether unintentional or due to carelessness, in the generated HTML can lead to spurious regex matches. My current Emacs Lisp solution is:
1. Collect the tags for a post.
2. <<my-create-tag-file-for-tag>>For each tag, if ~tags/<tag>/index.org~ exists, then append relative link to the post to it, else create that file and add relative link to the post to it.
3. Repeat this for all posts.
Linking tags from posts is handled by my header function which uses built-in Org variables. This bypasses both post-processing the generated HTML, and regex matches. When publishing, I just recursively delete the ~tags~ directory and recreate it. This gets around the issue of checking existing links to posts for duplicates.

* Publishing code
All the Emacs Lisp code are in ~publish.el~ at the root of my pages directory. The main functions are:
- ~my-html-preamble~ :: This creates the ~<header>~ element for each page. It is based on [[https://jeffkreeftmeijer.com/org-html-preamble-postamble/][this]]. It automatically adds the date that each post was published on. If the post was updated later then it also adds that date. Additionally it adds links to the tags for that post.
- ~my-sitemap-format-entry~ :: This function defines how each entry in the list of posts look like. It adds links to the tags for each post.
- ~my-create-tag-file-for-tag~ :: This function encodes the logic for step [[my-create-tag-file-for-tag]] of my new solution for tags. ~my-create-tag-files-from-post-file~ and ~my-create-tag-files-from-directory~ then loops this function over all tags and all posts.
  

#+include: "../../publish.el" src emacs-lisp

** Makefile

Finally I have a Makefile to semi-automate the whole process. It has directives to:
1. Run ~publish.el~ with Emacs in batch mode.
2. Delete the Org cache.
3. Delete the Org cache, then run ~publish.el~ with Emacs in batch mode.
However usually I just find myself doing =C-c C-e= in a buffer visiting ~publish.el~.

#+include: ../../Makefile src makefile
