<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-09-28 Sat 20:33 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Generalized bra-ket notation in LaTeX</title>
<meta name="generator" content="Org Mode" />
<link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>স</text></svg>">
<link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
<link rel="stylesheet" type="text/css" href="/css/syntax.css">
<link rel="stylesheet" type="text/css" href="/css/extra.css">
<script>
  window.MathJax = {
    tex: {
      ams: {
        multlineWidth: '85%'
      },
      tags: 'ams',
      tagSide: 'right',
      tagIndent: '.8em'
    },
    chtml: {
      scale: 1.0,
      displayAlign: 'center',
      displayIndent: '0em'
    },
    svg: {
      scale: 1.0,
      displayAlign: 'center',
      displayIndent: '0em'
    },
    output: {
      font: 'mathjax-modern',
      displayOverflow: 'overflow'
    }
  };
</script>

<script
  id="MathJax-script"
  async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
</script>
</head>
<body>
<header id="preamble" class="status">
<nav>
<a href="/" alt="Go home">home</a>
<a href="/posts/" alt="Read all posts">posts</a>
</nav>
<h1 class="title">Generalized bra-ket notation in LaTeX</h1>
<p class="date">Published on 2024-05-15 (updated on 2024-05-19)</p>
<p class="tags">tags: <a class="tag" href="../../tags/latex/index.html">latex</a></p>
</header>
<main id="content" class="content">
<p>
I wrote a small package, <a href="https://codeberg.org/e-eight/braketx"><code>braketx</code></a>, for generalized bra-ket notation in LaTeX (or TeX). It is based on Donald Arseneau&rsquo;s <code>braket</code> package. The <code>braket</code> package provides a few macros, mostly one-liners, for the usual bra-ket and set notations. Donald suggests to copy his macros and replace \(\langle\) and \(\rangle\) with the required delimiters for other bra-ket notations. I did that, but then generalized the definitions and made them into a &ldquo;package&rdquo; so that I don&rsquo;t have to copy the definitions every time I write a new document.
</p>

<p>
<code>braketx</code> provides 6 macros:
</p>
<dl class="org-dl">
<dt><code>\xbra</code></dt><dd><p>
This is a generalized bra. The delimiter is the first argument.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">xbra</span>#1#2{<span class="org-keyword">\mathinner</span>{#1{#2}|}}
</pre>
</div></dd>
<dt><code>\xket</code></dt><dd><p>
This is a generalized ket. The delimiter is the last argument.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">xket</span>#1#2{<span class="org-keyword">\mathinner</span>{|{#1}#2}}
</pre>
</div></dd>
<dt><code>\xbraket</code></dt><dd><p>
This is a generalized bra-ket inner product. The left delimiter is the first argument, and the right delimiter the second argument.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">xket</span>#1#2{<span class="org-keyword">\mathinner</span>{|{#1}#2}}
</pre>
</div></dd>
<dt><code>\xBra</code></dt><dd><p>
Same as <code>\xbra</code>, but delimiter adjusts to the size of the label.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">xBra</span>#1#2{<span class="org-keyword">\left</span>#1{#2}<span class="org-keyword">\right</span>|}
</pre>
</div></dd>
<dt><code>\xKet</code></dt><dd><p>
Same as <code>\xket</code>, but delimiter adjusts to the size of the label.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">xKet</span>#1#2{<span class="org-keyword">\left</span>|{#1}<span class="org-keyword">\right</span>#2}
</pre>
</div></dd>
<dt><code>\xBraket</code></dt><dd><p>
Same as <code>\xbraket</code>, but delimiters and any vertical or double vertical lines within adjust to size of the labels. This is not an one-liner. I couldn&rsquo;t have figured out the logic to adjust the size of any vertical lines between the delimiters. TeX is complex.
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\let\protect\relax</span>
<span class="org-comment-delimiter">%</span>
{<span class="org-keyword">\catcode</span>`<span class="org-keyword">\|</span>=<span class="org-keyword">\active</span>
  <span class="org-keyword">\xdef\xBraket</span>{<span class="org-keyword">\protect\expandafter\noexpand\csname</span> xBraket <span class="org-keyword">\endcsname</span>}
  <span class="org-keyword">\expandafter\gdef\csname</span> xBraket <span class="org-keyword">\endcsname</span>#1#2#3{<span class="org-keyword">\begingroup</span>
     <span class="org-keyword">\ifx\SavedDoubleVert\relax</span>
       <span class="org-keyword">\let\SavedDoubleVert\|\let\|\BraDoubleVert</span>
     <span class="org-keyword">\fi</span>
     <span class="org-keyword">\mathcode</span>`<span class="org-keyword">\|</span>32768<span class="org-keyword">\let</span>|<span class="org-keyword">\BraVert</span>
     <span class="org-keyword">\left</span>#1{#2}<span class="org-keyword">\right</span>#3<span class="org-keyword">\endgroup</span>}
}
<span class="org-keyword">\def</span>\<span class="org-function-name">BraVert</span>{<span class="org-keyword">\@ifnextchar</span>|{<span class="org-keyword">\|\@gobble</span>}<span class="org-comment">% turn || into \|</span>
     {<span class="org-keyword">\egroup\,\mid@vertical\,\bgroup</span>}}
<span class="org-keyword">\def</span>\<span class="org-function-name">BraDoubleVert</span>{<span class="org-keyword">\egroup\,\mid@dblvertical\,\bgroup</span>}
<span class="org-keyword">\let\SavedDoubleVert\relax</span>

<span class="org-keyword">\begingroup</span>
 <span class="org-keyword">\edef\@tempa</span>{<span class="org-keyword">\meaning\middle</span>}
 <span class="org-keyword">\edef\@tempb</span>{<span class="org-keyword">\string\middle</span>}
<span class="org-keyword">\expandafter</span> <span class="org-keyword">\endgroup</span> <span class="org-keyword">\ifx\@tempa\@tempb</span>
 <span class="org-keyword">\def</span>\<span class="org-function-name">mid@vertical</span>{<span class="org-keyword">\middle</span>|}
 <span class="org-keyword">\def</span>\<span class="org-function-name">mid@dblvertical</span>{<span class="org-keyword">\middle\SavedDoubleVert</span>}
<span class="org-keyword">\else</span>
 <span class="org-keyword">\def</span>\<span class="org-function-name">mid@vertical</span>{<span class="org-keyword">\mskip</span>1mu<span class="org-keyword">\vrule\mskip</span>1mu}
 <span class="org-keyword">\def</span>\<span class="org-function-name">mid@dblvertical</span>{<span class="org-keyword">\mskip</span>1mu<span class="org-keyword">\vrule\mskip</span>2.5mu<span class="org-keyword">\vrule\mskip</span>1mu}
 <span class="org-keyword">\fi</span>
</pre>
</div></dd>
</dl>

<p>
With this you can easily create bra-ket notation like \((a|b\rangle\), \(\{\sigma|\), or even, \[\left\{\frac{1}{2}\frac{1}{2}\middle|O\middle|\alpha\beta\right).\] One caveat of the fixed-size delimiter macros is that they do not check if the arguments that are supposed to be delimiters are actually delimiters. <code>\xket{a}{b}</code> is as valid as <code>\xket{a}{\rangle}</code>. But then I don&rsquo;t expect anyone to actually use <code>\xket{a}{b}</code>.
</p>

<p>
Admittedly this is a very niche package. But comes in quite handy when you are writing documents where you need generalized bra-ket notations to distinguish different types of quantum states. For example, distinguishing Slater determinants from product states.
</p>

<p>
To use it, you can just copy the definitions to a file <code>braketx</code> which you can then call with <code>\usepackage{braketx}</code>. Or clone the git repository and put it in your TeX path. On Linux machines I put custom packages under <code>~/.local/tex/latex/</code>, and run <code>sudo tlmgr conf auxtrees add ~/.local/texmf/</code>. You may or may not have to use <code>sudo</code>. You can use this alongside the <code>braket</code> package. Or you can redefine the <code>braket</code> in terms of the <code>braketx</code> macros. For example, you can define <code>\bra</code> as:
</p>
<div class="org-src-container">
<pre class="src src-latex"><span class="org-keyword">\def</span>\<span class="org-function-name">bra</span>#1{<span class="org-keyword">\xbra</span>{<span class="org-keyword">\langle</span>}{#1}}
</pre>
</div>
</main>
<footer id="postamble" class="status">
<p>Copyright &copy; 2020 &mdash; 2024 Soham Pal</p>
</footer>
</body>
</html>
