;;; Publishing configuration -*- lexical-binding: t -*-
(require 'package)
(package-initialize)
(require 'org)
(require 'ox)
(require 'ox-publish)

(setq org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
(org-babel-do-load-languages 'org-babel-load-languages
                             (append org-babel-load-languages
                                     '((ditaa . t))))

;;; header / footer
(defvar my-html-head-extra
  (concat
   "<link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>স</text></svg>\">\n"
   "<link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.min.css\">\n"
   "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/syntax.css\">\n"
   "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/extra.css\">\n"))

(defun my-html-preamble (info)
  (let* ((date-format "%Y-%m-%d")
         (file (plist-get info :input-file))
         (published (org-export-data
                     (org-export-get-date info date-format) info))
         (updated (format-time-string
                   date-format
                   (file-attribute-modification-time
                    (file-attributes file))))
          (title (org-get-title info)))
      (concat
       "<nav>\n"
       "<a href=\"/\" alt=\"Go home\">home</a>\n"
       "<a href=\"/posts/\" alt=\"Read all posts\">posts</a>\n"
       "</nav>\n"
       "<h1 class=\"title\">" title "</h1>\n"
       (unless (or (equal title "~")
                   (equal title "~/posts")
                   (string-match-p (regexp-quote "tags")
                                   (directory-file-name file)))
         (concat "<p class=\"date\">Published on " published
                 (unless (equal updated published)
                   (concat " (updated on " updated ")"))
                 "</p>\n"))
       (when org-file-tags
         (concat "<p class=\"tags\">tags: "
                 (mapconcat
                  '(lambda (tag)
                     (concat "<a class=\"tag\" href=\"../../tags/"
                             tag
                             "/index.html\">"
                             tag
                             "</a>"))
                  (flatten-list org-file-tags)
                  " ")
                 "</p>\n")))))

(defvar my-html-postamble
   (format-time-string "<p>Copyright &copy; 2020 &mdash; %Y Soham Pal</p>\n"))

;;; Sitemap
(defun my-sitemap-format-entry (entry style project)
  (let ((title (org-publish-find-title entry project))
        (tags (org-publish-find-property entry :filetags project)))
    (concat "[[file:" entry "][" title "]]"
            (when tags
              (concat
               " ("
               (mapconcat
                '(lambda (tag)
                   (concat "[[file:../tags/" tag "/index.org][" tag "]]"))
                (flatten-list tags)
                " ")
               ")")))))

;;; Tags
(defun my-create-tag-file-for-tag (tag post-file)
  (let* ((tag-file (file-name-concat "tags" tag "index.org"))
         (relpath (file-relative-name post-file (file-name-directory tag-file)))
         (title (org-get-title post-file)))
    (if (file-exists-p tag-file)
        (write-region
         (concat "- [[file:" relpath "][" title "]]\n")
         nil
         tag-file
         :append)
      (progn
        (make-directory (file-name-directory tag-file) :parents)
        (with-temp-file tag-file
          (insert (concat "#+title:" tag "\n"))
          (insert "\n")
          (insert (concat "- [[file:" relpath "][" title "]]\n")))))))

(defun my-create-tag-files-from-post-file (post-file)
  (let ((tags (with-temp-buffer
                (insert-file-contents post-file)
                (org-mode)
                 org-file-tags)))
    (when tags
      (dolist (tag (flatten-list tags))
        (my-create-tag-file-for-tag tag post-file)))))

(defun my-create-tag-files-from-directory (dir)
  (let ((files (directory-files-recursively dir ".org")))
    (dolist (file files)
      (my-create-tag-files-from-post-file file))))


;;; Common publish settings
(setq org-export-allow-bind-keywords t
      org-export-babel-evaluate nil
      org-export-with-author nil
      org-export-with-creator nil
      org-export-with-footnotes t
      org-export-with-latex t
      org-export-with-smart-quotes t
      org-export-with-section-numbers nil
      org-export-with-toc nil
      org-export-with-title nil
      org-html-container-element "main"
      org-html-doctype "html5"
      org-html-divs '((preamble "header" "preamble")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-head-include-default-style nil
      org-html-head-extra my-html-head-extra
      org-html-html5-fancy t
      org-html-htmlize-output-type "css"
      org-html-link-home ""
      org-html-link-up ""
      org-html-preamble 'my-html-preamble
      org-html-postamble my-html-postamble
      org-html-validation-link nil)

;;; Export
(setq org-publish-project-alist
      `(("org-posts"
         :base-directory "./posts/"
         :base-extension "org"
         :publishing-directory "./posts"
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-style list
         :sitemap-format-entry my-sitemap-format-entry
         :sitemap-sort-files anti-chronologically
         :sitemap-title "~/posts")
        ("org-tags"
         :base-directory "./tags/"
         :base-extension "org"
         :publishing-directory "./tags"
         :recursive t
         :publishing-function org-html-publish-to-html)
        ("org-home"
         :base-directory "./"
         :base-extension "org"
         :publishing-directory "./"
         :recursive nil
         :publishing-function org-html-publish-to-html)))

;;; Publish
(when (file-exists-p "tags")
  (delete-directory "tags" :recursive nil))
(my-create-tag-files-from-directory "posts")
(org-publish-all)
